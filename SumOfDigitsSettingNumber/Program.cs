﻿using System;

namespace SumOfDigitsSettingNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("a = ");
            long number = long.Parse(Console.ReadLine());
            long sum = 0;
            int step = 0;
            while (number > 0)
            {
                step = (int)(number % 10);
                number /= 10;
                sum += step;
            }
            Console.WriteLine(sum);


        }
    }
}
