﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            for(; ; )
            {
                Console.Write("number = ");
                int number = int.Parse(Console.ReadLine());
                int first = 1;
                int second = 1;
                Console.Write(second + "  ");
                Console.Write(first + "  ");
                int next = 0;
                for (int i = 2; i < number; i++)
                {
                    next = first + second;
                    Console.Write(next+"  ");
                    second = first;
                    first = next;
                }
                Console.WriteLine();
            }
        }
    }
}
