﻿using System;

namespace SumOfDigits

{
    class Program
    {
        static void Main(string[] args)
        {
            for(; ; )
            {
                Console.Write("number = ");
                int number = int.Parse(Console.ReadLine());
                int newNumber = number;
                int sum = 0;
                int digit = 0;
                int count = 0;
                while (number > 0)
                {
                    number = number / 10;
                    count++;
                }
                Console.WriteLine($"Count of Digits is {count}");
                int classifyOfNumber = 1;
                for (int i = 1; i < count; i++)
                {
                    classifyOfNumber *= 10;
                }
                for (int i = 0; i < count; i++)
                {
                    digit = newNumber / classifyOfNumber;
                    newNumber %= classifyOfNumber;
                    classifyOfNumber /= 10;
                    sum += digit;
                    Console.Write(digit+" , ");
                }
                Console.WriteLine();
                Console.WriteLine($"Sum Of Digits is {sum}");
            }
            
        }
    }
}
